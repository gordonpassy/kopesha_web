<?php
/**
 * Created by PhpStorm.
 * User: gordonpassy
 * Date: 27/07/2017
 * Time: 13:11
 */

/**
 * Http codes
 */
define('CREATED',201);
define('OK',200);
define('ERROR_EXISTS',403);
define('SERVER_ERROR',500);

define('COUNTRY_CODE','254');
define("BASE_URL",'http://104.131.181.105/kopesha/index.php/api/v1/');

//Status
define('REVIEW','Under Review');
define('PROCESSING','Requesting Being Processed');