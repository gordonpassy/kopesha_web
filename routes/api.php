<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::resource('/v1/register/', 'SignUpController');

Route::get('/v1/admin/','AdminController@index');
Route::post('/v1/admin/','AdminController@store');
Route::get('/v1/admin/{token}','AdminController@show');
Route::put('/v1/admin/{token}','AdminController@update');
Route::delete('/v1/admin/{token}','AdminController@destroy');

Route::resource('/v1/mpesa/', 'MpesaTransactionController');
Route::resource('/v1/message/', 'MessageController');
Route::post('/v1/send-sms/','SMSController@store');

Route::post('/v1/collateral/','CollateralController@store');

//Products
Route::post('/v1/product/','ProductController@store');
Route::get('/v1/products/','ProductController@index');

//Borrower
Route::post('/v1/borrower/','BorrowerController@store');

//Login
Route::post('/v1/borrower/login/','LoginController@store');
Route::post('/v1/admin/login','AdminLoginController@store');

//Loan
Route::post('/v1/loan/','LoanController@store');
Route::get('/v1/loan/{loan_id}','LoanController@show');
Route::get('/v1/borrower-loans/{borrower_id}','LoanController@index');


//Land Collateral
Route::post('/v1/land-collateral/','LandCollateralController@store');
Route::post('/v1/car-collateral/','CarCollateralController@store');

//Payments
Route::post('/v1/credit/','CreditController@store');
Route::post('/v1/debit/','DebitController@store');


//Images
Route::get('/v1/images/','ImageController@index');
Route::get('/v1/images/{image}','ImageController@show');






