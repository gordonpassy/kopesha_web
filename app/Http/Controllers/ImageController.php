<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class ImageController extends Controller
{
    //
    public function index()
    {
        //
        echo public_path();
    }


    public function show($image)
    {
        //
        try{
            $path=public_path().'/images/'.$image;
            return Image::make($path)->response();
        }catch (\Exception $exception){
            $errorMessage = array("error" => $exception->getMessage(), "code" => $exception->getCode());
            return response()->json($errorMessage,SERVER_ERROR);
        }
    }
}
