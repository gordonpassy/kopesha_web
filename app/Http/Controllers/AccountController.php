<?php

namespace App\Http\Controllers;

use App\Account;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try{
            $borrower_id=$request->input('borrower_id');
            $transaction_reference=$request->input('transaction_reference');
            $payment_code=$request->input('payment_code');
            $amount=$request->input('amount');
            $debit=$request->input('debit');
            $credit=$request->input('credit');
            $description=$request->input('description');
            $balance=$request->input('balance');
        }catch (\Exception $exception){
            $errorMessage = array("error" => $exception->getMessage(), "code" => $exception->getCode());
            return response()->json($errorMessage,SERVER_ERROR);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Account  $borrower_id
     * @return \Illuminate\Http\Response
     */
    public function show($borrower_id)
    {
        //
        try{
            $accounts=Account::where('borrower_id',$borrower_id)->get();
            return response()->json($accounts,200);
        }catch (\Exception $exception){
            $errorMessage = array("error" => $exception->getMessage(), "code" => $exception->getCode());
            return response()->json($errorMessage,SERVER_ERROR);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function edit(Account $account)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Account $account)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function destroy(Account $account)
    {
        //
    }
}
