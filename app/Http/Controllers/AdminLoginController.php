<?php

namespace App\Http\Controllers;

use App\Admin;
use Illuminate\Http\Request;
use Mockery\CountValidator\Exception;

class AdminLoginController extends Controller
{
    //

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try{
            $phone=$request->input('phone_number');
            $password=$request->input('password');
            $phoneNumber=COUNTRY_CODE.substr($phone,-9);

            $user=Admin::where('phone_number',$phoneNumber)->first();
            if($user!=null){
                if(password_verify($password,$user->password)){
                    return response()->json($user,OK);
                }else{
                    $message = array("message" => "Invalid credentials");
                    return response()->json($message,ERROR_EXISTS);
                }

            }else{
                $message = array("message" => "The number is not registered. Try creating an account");
                return response()->json($message,ERROR_EXISTS);
            }
        }catch (Exception $exception){
            $errorMessage = array("error" => $exception->getMessage(), "code" => $exception->getCode());
            return response()->json($errorMessage,SERVER_ERROR);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
