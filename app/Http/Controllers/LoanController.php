<?php

namespace App\Http\Controllers;

use App\Borrower;
use App\CarCollateral;
use App\LandCollateral;
use App\Loan;
use Illuminate\Http\Request;

class LoanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($borrower_id)
    {
        //
        try{
            $loans=Loan::where('borrower_id',$borrower_id)->orderBy('updated_at','desc')->get();
            foreach ($loans as $loan){
                $loan->product;
                $loan->landCollaterals;
                $loan->carCollaterals;
            }
            return response()->json($loans,200);
        }catch (\Exception $exception){
            $errorMessage = array("error" => $exception->getMessage(), "code" => $exception->getCode());
            return response()->json($errorMessage,SERVER_ERROR);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try{
            $token=$request->input('token');
            $product_id=$request->input('product_id');
            $amount=$request->input('amount');
            $repayment_period=$request->input('repayment_period');
            $purpose=$request->input('purpose');

            $borrower=Borrower::where('token',$token)->where('deleted',0)->first();
            if($borrower!=null){
                $loan=new Loan();
                $loan->borrower_id=$borrower->id;
                $loan->product_id=$product_id;
                $loan->amount=$amount;
                $loan->repayment_period=$repayment_period;
                $loan->loan_status=REVIEW;
                $loan->purpose=$purpose;

                $loan->save();

                $loan=Loan::find($loan->id);
                return response()->json($loan,201);
            }else{
                $message=array("error"=>"Invalid token");
                return response()->json($message,401);
            }

        }catch (\Exception $exception){
            $errorMessage = array("error" => $exception->getMessage(), "code" => $exception->getCode());
            return response()->json($errorMessage,SERVER_ERROR);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Loan  $loan_id
     * @return \Illuminate\Http\Response
     */
    public function show($loan_id)
    {
        //
        try{
            $loan=Loan::find($loan_id);
            $loan->product;
            $loan->landCollaterals;
            $loan->carCollaterals;
            return response()->json($loan,200);
        }catch (\Exception $exception){
            $errorMessage = array("error" => $exception->getMessage(), "code" => $exception->getCode());
            return response()->json($errorMessage,SERVER_ERROR);

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function edit(Loan $loan)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Loan $loan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Loan  $loan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Loan $loan)
    {
        //
    }
}
