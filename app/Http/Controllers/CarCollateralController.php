<?php

namespace App\Http\Controllers;

use App\CarCollateral;
use Illuminate\Http\Request;

class CarCollateralController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try{
            $borrower_id=$request->input('borrower_id');
            $loan_id=$request->input('loan_id');
            $make=$request->input('vehicle_make');
            $model=$request->input('vehicle_model');
            $year_of_manufacture=$request->input('year_of_manufacture');

            $carCollateral=new CarCollateral();
            $carCollateral->borrower_id=$borrower_id;
            $carCollateral->loan_id=$loan_id;
            $carCollateral->vehicle_make=$make;
            $carCollateral->vehicle_model=$model;
            $carCollateral->year_of_manufacture=$year_of_manufacture;

            $carCollateral->save();
            return response()->json($carCollateral,201);
        }catch (\Exception $exception){
            $errorMessage = array("error" => $exception->getMessage(), "code" => $exception->getCode());
            return response()->json($errorMessage,SERVER_ERROR);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CarCollateral  $carCollateral
     * @return \Illuminate\Http\Response
     */
    public function show(CarCollateral $carCollateral)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CarCollateral  $carCollateral
     * @return \Illuminate\Http\Response
     */
    public function edit(CarCollateral $carCollateral)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CarCollateral  $carCollateral
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CarCollateral $carCollateral)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CarCollateral  $carCollateral
     * @return \Illuminate\Http\Response
     */
    public function destroy(CarCollateral $carCollateral)
    {
        //
    }
}
