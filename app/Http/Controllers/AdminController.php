<?php

namespace App\Http\Controllers;

use App\Admin;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        try{
            $admins=Admin::all();
            return response()->json($admins,200);
        }catch (\Exception $exception){
            $errorMessage = array("error" => $exception->getMessage(), "code" => $exception->getCode());
            return response()->json($errorMessage);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try{
            $name=$request->input('name');
            $email=$request->input('email');
            $phoneNumber=COUNTRY_CODE.substr($request->input('phone_number'),-9);
            $password=bcrypt($request->input('password'));
            $token= generateToken($phoneNumber);


            $admin=Admin::where('phone_number',$phoneNumber)->orWhere('email',$email)->first();
            if($admin!=null){
                $message=array("error"=>"There is a user already with the phone number or email.");
                return response()->json($message,401);
            }else{
                $admin=new Admin();
                $admin->name=$name;
                $admin->email=$email;
                $admin->phone_number=$phoneNumber;
                $admin->password=$password;
                $admin->token=$token;

                $admin->save();

                return response()->json($admin,201);
            }
        }catch (\Exception $exception){
            $errorMessage = array("error" => $exception->getMessage(), "code" => $exception->getCode());
            return response()->json($errorMessage,SERVER_ERROR);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  $token
     * @return \Illuminate\Http\Response
     */
    public function show($token)
    {
        //
        try{
            $admin=Admin::where('token',$token)->where('deleted',0)->first();
            if($admin!=null){
                return response()->json($admin,200);
            }else{
                $message=array("error"=>"Invalid details.");
                return response()->json($message,401);
            }
        }catch (\Exception $exception){
            $errorMessage = array("error" => $exception->getMessage(), "code" => $exception->getCode());
            return response()->json($errorMessage,SERVER_ERROR);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $admin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $token
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $token)
    {
        //
        try{
            $admin=Admin::where('token',$token)->where('deleted',0)->first();
            if($admin!=null){
                $name=$request->input('name');
                $email=$request->input('email');
                $phoneNumber=$request->input('phone_number');

                if($name!=null){
                    $admin->name=$name;
                }
                if($email!=null){
                    $admin->email=$email;
                }
                if($phoneNumber!=null){
                    $admin->phone_number=$phoneNumber;
                }
                $admin->save();
                return response()->json($admin,200);
            }else{
                $message=array("error"=>"Invalid details.");
                return response()->json($message,401);
            }
        }catch (\Exception $exception){
            $errorMessage = array("error" => $exception->getMessage(), "code" => $exception->getCode());
            return response()->json($errorMessage);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $token
     * @return \Illuminate\Http\Response
     */
    public function destroy($token)
    {
        //
        try{
            $admin=Admin::where('token',$token)->where('deleted',0)->first();
            if($admin!=null){
                $admin->delete();
                $message=array("message"=>"Record Deleted.");
                return response()->json($message,200);
            }else{
                $message=array("error"=>"Invalid details.");
                return response()->json($message,401);
            }
        }catch (\Exception $exception){
            $errorMessage = array("error" => $exception->getMessage(), "code" => $exception->getCode());
            return response()->json($errorMessage);
        }
    }
}
