<?php

namespace App\Http\Controllers;

use App\LandCollateral;
use Illuminate\Http\Request;

class LandCollateralController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try{
            $borrower_id=$request->input('borrower_id');
            $loan_id=$request->input('loan_id');
            $lr_no=$request->input('lr_no');
            $size=$request->input('size');
            $unit=$request->input('unit');

            $landCollateral=new LandCollateral();
            $landCollateral->borrower_id=$borrower_id;
            $landCollateral->loan_id=$loan_id;
            $landCollateral->lr_no=$lr_no;
            $landCollateral->size=$size;
            $landCollateral->unit=$unit;

            $landCollateral->save();
            return response()->json($landCollateral,201);

        }catch (\Exception $exception){
            $errorMessage = array("error" => $exception->getMessage(), "code" => $exception->getCode());
            return response()->json($errorMessage,SERVER_ERROR);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LandCollateral  $landCollateral
     * @return \Illuminate\Http\Response
     */
    public function show(LandCollateral $landCollateral)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LandCollateral  $landCollateral
     * @return \Illuminate\Http\Response
     */
    public function edit(LandCollateral $landCollateral)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LandCollateral  $landCollateral
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LandCollateral $landCollateral)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LandCollateral  $landCollateral
     * @return \Illuminate\Http\Response
     */
    public function destroy(LandCollateral $landCollateral)
    {
        //
    }
}
