<?php

namespace App\Http\Controllers;

use App\Account;
use App\Borrower;
use Illuminate\Http\Request;
use GuzzleHttp;

class CreditController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            $reference = $request->input('reference');
            $token = $request->input('token');

            $borrower = Borrower::where('token', $token)->first();

            if ($borrower != null) {
                $mpesa = MpesaTransactionController::where('transaction_reference', $reference)->orWhere('payment_code', $reference)->first();

                if ($mpesa != null) {
                    $account = Account::where('transaction_reference', $reference)->orWhere('payment_code', $reference)->first();
                    if ($account != null) {
                        $message = array("error" => "This transaction reference has already been used.");
                        return response()->json($message, 401);
                    } else {
                        $account = new Account();
                        $account->borrower_id =$borrower->id;
                        $account->transaction_reference=$mpesa->transaction_reference;
                        $account->payment_code=$mpesa->payment_code;
                        $account->amount=$mpesa->amount;
                        $account->credit=$mpesa->amount;
                        $account->description='Mpesa Deposit';
                        $account->balance=$borrower->account_balance+$mpesa->amount;

                        $account->save();
                        $borrower->account_balance=$account->balance;
                        $borrower->save();

                        return response()->json($account, 201);
                    }
                } else {
                    $message = array("error" => "Invalid transaction reference.");
                    return response()->json($message, 401);
                }
            } else {
                $message = array("error" => "No user registered with this account.");
                return response()->json($message, 401);
            }


        } catch (\Exception $exception) {
            $errorMessage = array("error" => $exception->getMessage(), "code" => $exception->getCode());
            return response()->json($errorMessage,SERVER_ERROR);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function creditAccount()
    {

    }
}
