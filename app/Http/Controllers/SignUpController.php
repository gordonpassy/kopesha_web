<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Mockery\CountValidator\Exception;

class SignUpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try{
            $name=$request->input('name');
            $phone=$request->input('phone');
            $idNumber=$request->input('id_number');
            $email=$request->input('email');
            $password=bcrypt($request->input('password'));
            $token=generateToken($phone);

            $phoneNumber=COUNTRY_CODE.substr($phone,-9);
            $user=User::where('token',$token)->first();
            if($user==null){
                $user=new User();
                $user->name=$name;
                $user->phone_number=$phoneNumber;
                $user->id_number=$idNumber;
                $user->email=$email;
                $user->password=$password;
                $user->token=$token;
                $user->save();

                return response()->json($user,CREATED);
            }else{
                $message = array("message" => "You are already registered!");
                return response()->json($message,ERROR_EXISTS);
            }
        }catch (\Exception $exception){
            $errorMessage = array("error" => $exception->getMessage(), "code" => $exception->getCode());
            return response()->json($errorMessage,SERVER_ERROR);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
