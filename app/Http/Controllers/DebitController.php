<?php

namespace App\Http\Controllers;

use App\Account;
use App\Borrower;
use App\Loan;
use App\Product;
use Illuminate\Http\Request;

class DebitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try{
            $token=$request->input('token');
            $loan_id=$request->input('loan_id');

            $loan=Loan::find($loan_id);
            $borrower=Borrower::where('token',$token)->first();
            $product=Product::find($loan->product_id);
            $price=$product->price;

            $borrower_id=$borrower->id;
            $payment_reference=generateTransactionId($borrower_id,$loan_id);



            if($loan->payment_status==FALSE){
                if($borrower->account_balance >= $price){

                    $account=new Account();
                    $account->borrower_id=$borrower_id;
                    $account->transaction_reference=$payment_reference;
                    $account->amount=$price;
                    $account->debit=$price;
                    $account->description='Payment for '.$product->name;
                    $account->balance=$borrower->account_balance-$price;
                    $account->save();

                    $borrower->account_balance=$account->balance;
                    $borrower->save();

                    $loan->payment_reference=$payment_reference;
                    $loan->payment_status=TRUE;
                    $loan->loan_status=PROCESSING;
                    $loan->save();


                    return response()->json($account, 201);
                }else{
                    $deficit=$price-$borrower->account_balance;
                    $message = array("error" => "You have insufficient balance.Ksh ".$deficit." required.");
                    return response()->json($message, 401);
                }

            }else{
                $message = array("error" => "Payment for this transaction has already been made.");
                return response()->json($message, 401);
            }

        }catch (\Exception $exception){
            $errorMessage = array("error" => $exception->getMessage(), "code" => $exception->getCode());
            return response()->json($errorMessage,SERVER_ERROR);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
