<?php

namespace App\Http\Controllers;

use App\MpesaTransaction;
use Illuminate\Http\Request;

class MpesaTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try{
            $serviceName=$request->input('service_name');
            $businessNumber=$request->input('business_number');
            $transactionReference=$request->input('transaction_reference');
            $internalId=$request->input('internal_transaction_id');
            $transactionTimestamp=$request->input('transaction_timestamp');
            $transactionType=$request->input('transaction_type');
            $senderPhone=$request->input('sender_phone');
            $firstName=$request->input('first_name');
            $middleName=$request->input('middle_name');
            $lastName=$request->input('last_name');
            $amount=$request->input('amount');
            $currency=$request->input('currency');
            $signature=$request->input('signature');
            $paymentCode=hexdec(substr(sha1($transactionReference),1,5));

            $mpesaTransaction=new MpesaTransaction();
            $mpesaTransaction->service_name=$serviceName;
            $mpesaTransaction->business_number=$businessNumber;
            $mpesaTransaction->transaction_reference=$transactionReference;
            $mpesaTransaction->internal_transaction_id=$internalId;
            $mpesaTransaction->transaction_timestamp=$transactionTimestamp;
            $mpesaTransaction->transaction_type=$transactionType;
            $mpesaTransaction->sender_phone=$senderPhone;
            $mpesaTransaction->first_name=$firstName;
            $mpesaTransaction->middle_name=$middleName;
            $mpesaTransaction->last_name=$lastName;
            $mpesaTransaction->amount=$amount;
            $mpesaTransaction->currency=$currency;
            $mpesaTransaction->signature=$signature;
            $mpesaTransaction->payment_code=$paymentCode;

            $mpesaTransaction->save();
            $message = "Payment of $currency " . $amount . " for Kopesha has been received. Your Payment Code is: " . $paymentCode;
            sendSMS($senderPhone,$message);
            return response()->json($mpesaTransaction,201);
        }catch (\Exception $exception){
            $errorMessage = array("error" => $exception->getMessage(), "code" => $exception->getCode());
            return response()->json($errorMessage,SERVER_ERROR);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MpesaTransaction  $mpesaTransaction
     * @return \Illuminate\Http\Response
     */
    public function show(MpesaTransaction $mpesaTransaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MpesaTransaction  $mpesaTransaction
     * @return \Illuminate\Http\Response
     */
    public function edit(MpesaTransaction $mpesaTransaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MpesaTransaction  $mpesaTransaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MpesaTransaction $mpesaTransaction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MpesaTransaction  $mpesaTransaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(MpesaTransaction $mpesaTransaction)
    {
        //
    }
}
