<?php

namespace App\Http\Controllers;

use App\Borrower;
use Illuminate\Http\Request;
use Mockery\CountValidator\Exception;

class BorrowerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try{
            $name=$request->input('name');
            $idNumber=$request->input('id_number');
            $phoneNumber=COUNTRY_CODE.substr($request->input('phone_number'),-9);
            $email=$request->input('email');
            $password=bcrypt($request->input('password'));
            $token=generateToken($phoneNumber);

            $borrower=Borrower::where('phone_number',$phoneNumber)->orWhere('email',$email)->first();
            if($borrower!=null){
                $message=array("error"=>"There is a user already with the phone number or email.");
                return response()->json($message,401);
            }else{
                $borrower=new Borrower();
                $borrower->name=$name;
                $borrower->id_number=$idNumber;
                $borrower->phone_number=$phoneNumber;
                $borrower->email=$email;
                $borrower->password=$password;
                $borrower->token=$token;

                $borrower->save();
                return response()->json($borrower,201);
            }

        }catch (\Exception $exception){
            $errorMessage = array("error" => $exception->getMessage(), "code" => $exception->getCode());
            return response()->json($errorMessage,SERVER_ERROR);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  $token
     * @return \Illuminate\Http\Response
     */
    public function show($token)
    {
        //
        try{
            $borrower=Borrower::where('token',$token)->where('deleted',0)->first();
            if($borrower!=null){
                return response()->json($borrower,200);
            }else{
                $message=array("error"=>"User does not exist.");
                return response()->json($message,401);
            }
        }catch (\Exception $exception){
            $errorMessage = array("error" => $exception->getMessage(), "code" => $exception->getCode());
            return response()->json($errorMessage,SERVER_ERROR);
        }


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Borrower  $borrower
     * @return \Illuminate\Http\Response
     */
    public function edit(Borrower $borrower)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Borrower  $borrower
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Borrower $borrower)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Borrower  $borrower
     * @return \Illuminate\Http\Response
     */
    public function destroy(Borrower $borrower)
    {
        //
    }
}
