<?php

namespace App\Http\Controllers;

use App\Collateral;
use Illuminate\Http\Request;

class CollateralController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try{
            $description=$request->input('description');
            $collateral=Collateral::where('description',$description)->first();
            if($collateral!=null){
                $message = array("error" => "The collateral type already exists.");
                return response()->json($message, 401);
            }else{
                $collateral=new Collateral();
                $collateral->description=$description;
                $collateral->save();
                return response()->json($collateral, 201);
            }

        }catch (\Exception $exception){
            $errorMessage = array("error" => $exception->getMessage(), "code" => $exception->getCode());
            return response()->json($errorMessage,SERVER_ERROR);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Collateral  $collateral
     * @return \Illuminate\Http\Response
     */
    public function show(Collateral $collateral)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Collateral  $collateral
     * @return \Illuminate\Http\Response
     */
    public function edit(Collateral $collateral)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Collateral  $collateral
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Collateral $collateral)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Collateral  $collateral
     * @return \Illuminate\Http\Response
     */
    public function destroy(Collateral $collateral)
    {
        //
    }
}
