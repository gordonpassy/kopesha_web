<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $hidden = [
        'deleted'
    ];
    public function user(){
        return $this->belongsTo('App\Admin');
    }

    public function collateral(){
        return $this->belongsTo('App\Collateral');
    }

    public function loans(){
        return $this->hasMany('App\Loan');
    }
}
