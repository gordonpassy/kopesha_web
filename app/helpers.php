<?php
/**
 * Created by PhpStorm.
 * User: gordonpassy
 * Date: 27/07/2017
 * Time: 12:54
 */
function generateToken($phoneNumber){
    return sha1($phoneNumber);
}

function generateTransactionId($borrowerId,$loanId){
    $reference=sha1($borrowerId.$loanId);
    return 'TX-'.hexdec(substr($reference,1,5));
}

function sendSMS($recipient,$message){
    $gateway=new \App\AfricasTalkingGateway();
    $results=$gateway->sendMessage($recipient,$message);
    foreach ($results as $result){
        $number=$result->number;
        $status=$result->status;
        $messageId=$result->messageId;
        $cost=$result->cost;

        $message=new \App\Message();
        $message->number = $number;
        $message->status = $status;
        $message->messageId = $messageId;
        $message->cost = $cost;
        $message->save();
    }
}