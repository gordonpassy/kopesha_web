<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    //
    protected $hidden = [
        'deleted'
    ];
    public function borrower(){
        return $this->belongsTo('App\Borrower');
    }
}
