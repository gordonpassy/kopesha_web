<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    //
    protected $hidden = [
        'deleted'
    ];
    public function product(){
        return $this->belongsTo('App\Product');
    }

    public function borrower(){
        return $this->belongsTo('App\Borrower');
    }

    public function landCollaterals(){
        return $this->hasMany('App\LandCollateral');
    }

    public function carCollaterals(){
        return $this->hasMany('App\CarCollateral');
    }
}
