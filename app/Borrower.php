<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Borrower extends Model
{
    //
    protected $hidden = [
        'password','deleted'
    ];
    public function loans(){
        return $this->hasMany('App\Loan');
    }

    public function landCollaterals(){
        return $this->hasMany('App\LandCollateral');
    }

    public function carCollaterals(){
        return $this->hasMany('App\CarCollateral');
    }

    public function statements(){
        return $this->hasMany('App\Account');
    }
}
