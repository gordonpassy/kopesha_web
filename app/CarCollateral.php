<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarCollateral extends Model
{
    //
    protected $hidden = [
        'deleted'
    ];
    public function loan(){
        return $this->belongsTo('App\Loan');
    }

    public function borrower()
    {
        return $this->belongsTo('App\Borrower');
    }}
