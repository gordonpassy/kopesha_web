<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarCollateralsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_collaterals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('borrower_id')->index()->unsigned();
            $table->integer('loan_id')->index()->unsigned();
            $table->string('vehicle_make');
            $table->string('vehicle_model');
            $table->integer('year_of_manufacture');
            $table->integer('deleted')->default(0);
            $table->foreign('borrower_id')->references('id')->on('borrowers')->onDelete('cascade');;
            $table->foreign('loan_id')->references('id')->on('loans')->onDelete('cascade');;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_collaterals');
    }
}
