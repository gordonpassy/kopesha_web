<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLandCollateralsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('land_collaterals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('borrower_id')->index()->unsigned();
            $table->integer('loan_id')->index()->unsigned();
            $table->string('lr_no');
            $table->float('size');
            $table->string('unit');
            $table->integer('deleted')->default(0);
            $table->foreign('borrower_id')->references('id')->on('land_collaterals')->onDelete('cascade');;
            $table->foreign('loan_id')->references('id')->on('loans')->onDelete('cascade');;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('land_collaterals');
    }
}
