<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('borrower_id')->index()->unsigned();
            $table->integer('product_id')->index()->unsigned();
            $table->double('amount')->nullable();
            $table->integer('repayment_period')->nullable();
            $table->string('purpose')->nullable();
            $table->string('payment_reference')->nullable();
            $table->boolean('payment_status')->default(false);
            $table->boolean('delivery_status')->default(false);
            $table->string('loan_status')->nullable();
            $table->integer('deleted')->default(0);
            $table->foreign('borrower_id')->references('id')->on('borrowers')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loans');
    }
}
