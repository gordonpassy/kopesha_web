<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admin_id')->index()->unsigned();
            $table->integer('collateral_id')->index()->unsigned()->nullable();
            $table->string('name');
            $table->string('description')->nullable();
            $table->string('color')->nullable();
            $table->string('icon')->nullable();
            $table->double('price');
            $table->integer('deleted')->default(0);
            $table->timestamps();
            $table->foreign('collateral_id')
                ->references('id')
                ->on('collaterals')->onDelete('cascade');
            $table->foreign('admin_id')
                ->references('id')
                ->on('admins')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
