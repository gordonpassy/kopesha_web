<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMpesaTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mpesa_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('service_name');
            $table->string('business_number');
            $table->string('transaction_reference')->unique();
            $table->string('payment_code')->unique()->nullable();
            $table->string('internal_transaction_id')->unique();
            $table->string('transaction_timestamp');
            $table->string('transaction_type');
            $table->string('sender_phone');
            $table->string('first_name')->nullable();
            $table->string('middle_name')->nullable();
            $table->string('last_name')->nullable();
            $table->double('amount');
            $table->string('currency');
            $table->string('signature');
            $table->integer('deleted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mpesa_transactions');
    }
}
