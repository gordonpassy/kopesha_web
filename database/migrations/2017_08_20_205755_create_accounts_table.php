<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('borrower_id')->index()->unsigned();
            $table->string('transaction_reference')->index();
            $table->string('payment_code')->unique()->nullable();
            $table->double('amount');
            $table->double('debit')->default(0);
            $table->double('credit')->default(0);
            $table->string('description')->nullable();
            $table->double('balance')->default(0);
            $table->integer('deleted')->default(0);
            $table->foreign('borrower_id')->references('id')->on('borrowers')->onDelete('cascade');;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts');
    }
}
